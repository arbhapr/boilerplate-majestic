<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.partials.head')
    </head>
    <body>
        <div class="container-scroller">
            @include('layouts.partials.navbar')
            <div class="container-fluid page-body-wrapper">
                @include('layouts.partials.sidebar')
                <div class="main-panel">
                    <div class="content-wrapper">
                        @include('flash-message')
                        @yield('content')
                    </div>
                    @include('layouts.partials.footer')
                </div>
            </div>
        </div>
        @include('layouts.partials.script')
    </body>
</html>