<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@yield('meta')

<title>SI-PEA</title>

<!-- plugins:css -->
<link rel="stylesheet" href="{{asset('vendor/majestic')}}/vendors/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="{{asset('vendor/majestic')}}/vendors/base/vendor.bundle.base.css">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet" href="{{asset('vendor/majestic')}}/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{asset('vendor/majestic')}}/css/style.css">
<!-- endinject -->
<link rel="shortcut icon" href="{{asset('vendor/majestic')}}/images/favicon.png" />
@yield('link')
@yield('style')