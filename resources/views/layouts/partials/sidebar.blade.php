<nav class="sidebar sidebar-offcanvas" id="sidebar">
	<ul class="nav">
		<li class="nav-item">
			<a class="nav-link" href="{{route('dashboard')}}">
				<i class="mdi mdi-home menu-icon"></i>
				<span class="menu-title">Dashboard</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="collapse" href="#master-data" aria-expanded="false" aria-controls="master-data">
				<i class="mdi mdi-circle-outline menu-icon"></i>
				<span class="menu-title">Master Data</span>
				<i class="menu-arrow"></i>
			</a>
			<div class="collapse" id="master-data">
				<ul class="nav flex-column sub-menu">
					<li class="nav-item">
						<a class="nav-link" href="{{route('master.users')}}">Users</a>
					</li>
				</ul>
			</div>
		</li>
	</ul>
</nav>