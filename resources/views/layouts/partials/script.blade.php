<!-- plugins:js -->
<script src="{{asset('vendor/majestic')}}/vendors/base/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{asset('vendor/majestic')}}/vendors/chart.js/Chart.min.js"></script>
<script src="{{asset('vendor/majestic')}}/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="{{asset('vendor/majestic')}}/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{asset('vendor/majestic')}}/js/off-canvas.js"></script>
<script src="{{asset('vendor/majestic')}}/js/hoverable-collapse.js"></script>
<script src="{{asset('vendor/majestic')}}/js/template.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('vendor/majestic')}}/js/dashboard.js"></script>
<script src="{{asset('vendor/majestic')}}/js/data-table.js"></script>
<script src="{{asset('vendor/majestic')}}/js/jquery.dataTables.js"></script>
<script src="{{asset('vendor/majestic')}}/js/dataTables.bootstrap4.js"></script>
<!-- End custom js for this page-->
@yield('script')