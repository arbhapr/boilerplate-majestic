<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Majestic Admin</title>
		<link rel="stylesheet" href="{{asset('vendor/majestic')}}/vendors/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" href="{{asset('vendor/majestic')}}/vendors/base/vendor.bundle.base.css">
		<link rel="stylesheet" href="{{asset('vendor/majestic')}}/css/style.css">
		<link rel="shortcut icon" href="{{asset('vendor/majestic')}}/images/favicon.png" />
	</head>
	<body>
		<div class="container-scroller">
			<div class="container-fluid page-body-wrapper full-page-wrapper">
				<div class="content-wrapper d-flex align-items-center auth px-0">
          			@yield('content')
				</div>
			</div>
		</div>

		<script src="{{asset('vendor/majestic')}}/vendors/base/vendor.bundle.base.js"></script>

		<script src="{{asset('vendor/majestic')}}/js/off-canvas.js"></script>
		<script src="{{asset('vendor/majestic')}}/js/hoverable-collapse.js"></script>
		<script src="{{asset('vendor/majestic')}}/js/template.js"></script>

	</body>
</html>

