@extends('layouts.panel')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin">
		<div class="d-flex justify-content-between flex-wrap">
			<div class="d-flex align-items-end flex-wrap">
				<div class="mr-md-3 mr-xl-5">
					<h2>Users</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Master</p>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Users</p>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Edit</p>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;{{$user->id}}</p>
                    </div>
                    <div class="d-flex mt-2">
                        <a href="{{ route('master.users') }}" class="btn btn-primary">Back</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<p class="card-title">Create New User</p>
                @include('warning')
                <form action="{{ route('master.users.update', $user->id) }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <label>Password Confirmation</label>
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role" required>
                            <option value="admin">Admin</option>
                            <option value="member" selected>Member</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                </form>
            </div>
		</div>
	</div>
</div>
@endsection