@extends('layouts.panel')

@section('content')
<div class="row">
	<div class="col-md-12 grid-margin">
		<div class="d-flex justify-content-between flex-wrap">
			<div class="d-flex align-items-end flex-wrap">
				<div class="mr-md-3 mr-xl-5">
					<h2>Users</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Master</p>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Users</p>
                    </div>
                    <div class="d-flex mt-2">
                        <a href="{{ route('master.users.create') }}" class="btn btn-primary">Create new User</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<p class="card-title">Users</p>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th>Name</th>
								<th>Email</th>
								<th width="10%">Role</th>
								<th width="5%">Options</th>
							</tr>
						</thead>
						<tbody>
                            @foreach($users as $n => $user)
                            <tr>
                                <td>{{ $n+1 }}</td>
                                <td>{{ ucwords($user->name) }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ ucwords($user->role) }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a type="button" class="btn btn-inverse-warning" href="{{route('master.users.edit', $user->id)}}">
                                            <i class="mdi mdi-pencil"></i>
                                        </a>
										<form method="POST" action="{{route('master.users.delete', $user->id)}}">
											@csrf
											@method('delete')
											<a type="submit" class="btn btn-inverse-danger delete-user" href="#">
												<i class="mdi mdi-delete"></i>
											</a>
										</form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
    $('.delete-user').click(function(e){
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('Are you sure?')) {
            // Post the form
            $(e.target).closest('form').submit() // Post the surrounding form
        }
    });
</script>
@endsection