@if($errors->any())
    <div class="alert alert-danger text-small text-danger pb-0">
        <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif