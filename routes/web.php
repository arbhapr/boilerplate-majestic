<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/auth.php';

Route::get('/', [MainController::class, 'index'])->name('dashboard');

Route::prefix('master')->name('master.')->group(function () {
    Route::get('user', [UserController::class, 'index'])->name('users');
    Route::get('user/create', [UserController::class, 'create'])->name('users.create');
    Route::post('user/store', [UserController::class, 'store'])->name('users.store');
    Route::get('user/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
    Route::post('user/update/{id}', [UserController::class, 'update'])->name('users.update');
    Route::delete('user/{id}', [UserController::class, 'destroy'])->name('users.delete');
});

// Route::resource('master/user', [UserController::class]);
